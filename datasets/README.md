### The data in each csv is encoded with integers, and if you want to find what integer corresponds to a value you can find it in the file [encoding_yagofacts.txt](https://gitlab.inria.fr/tyrex-public/rlqdag/-/blob/main/datasets/encoding_yagofacts.txt). 

#### To add all .csv files to PostgreSQL and create an index for each column you can run the following line (it uses the jar and delimiter.json file you see in  `datasets` folder):

```bash
java -jar benchmark-loader-postgres-1.0-SNAPSHOT-jar-with-dependencies.jar bulk-load --csv-format-json delimiter.json -i yago/
```

 


