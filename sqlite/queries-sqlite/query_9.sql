CREATE   VIEW const_subquery2 AS
  SELECT col2, trg FROM (SELECT * FROM (SELECT col2, col1 FROM (SELECT    src AS col2, trg AS col1 FROM actedIn) AS t) AS t1 NATURAL JOIN (SELECT trg, col1 FROM (SELECT    src AS trg, trg AS col1 FROM actedIn) AS t) AS t2) AS t;
CREATE VIEW fixpoint_relation1_X1 AS 
WITH RECURSIVE fixpoint_relation1_X1 (x, trg) AS (
SELECT x, trg FROM (SELECT x, trg FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT    src AS x, trg AS col1 FROM actedIn) AS t) AS t1 NATURAL JOIN (SELECT trg, col1 FROM (SELECT    src AS trg, trg AS col1 FROM actedIn) AS t) AS t2) AS t) AS const
UNION
SELECT fixpoint_relation1_X1.x, const_subquery2.trg FROM fixpoint_relation1_X1 JOIN const_subquery2 ON fixpoint_relation1_X1.trg = const_subquery2.col2
)
SELECT DISTINCT * FROM fixpoint_relation1_X1
;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t WHERE trg = '99436162') AS t) AS t;