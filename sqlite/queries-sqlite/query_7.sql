CREATE   VIEW const_subquery2 AS
  SELECT col2, col5 FROM (SELECT    src AS col2, trg AS col5 FROM isLocatedIn) AS t;
CREATE VIEW fixpoint_relation1_X1 AS 
WITH RECURSIVE fixpoint_relation1_X1 (col3, col5) AS(
    SELECT col3, col5 FROM (SELECT col3, col5 FROM (SELECT src AS col3, trg AS col5 FROM isLocatedIn) AS t) AS const
  UNION 
    SELECT fixpoint_relation1_X1.col3, const_subquery2.col5 FROM fixpoint_relation1_X1 JOIN const_subquery2 ON fixpoint_relation1_X1.col5 = const_subquery2.col2
)
SELECT DISTINCT * FROM fixpoint_relation1_X1
; 
CREATE   VIEW const_subquery4 AS
  SELECT col4, trg FROM (SELECT    src AS col4, trg FROM dealsWith) AS t;
CREATE VIEW fixpoint_relation3_X2 AS 
WITH RECURSIVE fixpoint_relation3_X2 (col5, trg) AS (
    SELECT col5, trg FROM (SELECT col5, trg FROM (SELECT src AS col5, trg FROM dealsWith) AS t) AS const
  UNION 
    SELECT fixpoint_relation3_X2.col5, const_subquery4.trg FROM fixpoint_relation3_X2 JOIN const_subquery4 ON fixpoint_relation3_X2.trg = const_subquery4.col4
)
SELECT DISTINCT * FROM fixpoint_relation3_X2
;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, trg FROM (SELECT * FROM (SELECT x, col5 FROM (SELECT * FROM (SELECT x, col3 FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT    src AS x, trg AS col1 FROM hasAcademicAdvisor) AS t) AS t1 NATURAL JOIN (SELECT col1, col3 FROM (SELECT    src AS col1, trg AS col3 FROM livesIn) AS t) AS t2) AS t) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation1_X1) AS t2) AS t) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation3_X2) AS t2) AS t) AS t WHERE trg = '42262378') AS t) AS t;