#!/bin/bash

# Directory containing the SQL query files
QUERIES_DIR="$1"

# SQLite database file
DB_FILE="$2"

# Loop through query files from query_1 to query_9
for ((i = 1; i <= 9; i++)); do
    QUERY_FILE="${QUERIES_DIR}/query_${i}.sql"

    # Check if the query file exists
    if [ -f "$QUERY_FILE" ]; then
        # Execute the query and measure execution time
        start=$(date +%s%N)
        sqlite3 "${DB_FILE}" < "$QUERY_FILE" > /dev/null
        end=$(date +%s%N)

        # Calculate execution time in milliseconds
        execution_time=$((($end - $start) / 1000000))
        
        # Get all views from the SQLite database
        views=$(sqlite3 "$DB_FILE" "SELECT name FROM sqlite_master WHERE type='view';")

        # Loop through each view and drop it
        for view in $views
        do
            sqlite3 "$DB_FILE" "DROP VIEW IF EXISTS $view;"
            echo "Dropped view: $view"
        done
        
        # Save execution time to result file
        echo "Query ${i} Execution Time (ms): ${execution_time}" > "${QUERIES_DIR}/result_${i}"
    else
        echo "Query file query_${i} not found."
    fi
done
