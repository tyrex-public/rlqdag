#!/bin/bash

# Variables
CSV_FOLDER="$1"
DB_NAME="$2"

# Check if the database file already exists
if [ -f "$DB_NAME" ]; then
    echo "Database file $DB_NAME already exists."
else
    echo "Creating SQLite database $DB_NAME..."
    touch "$DB_NAME"
fi

# Get all CSV files in the folder
csv_files=("$CSV_FOLDER"/*.csv)

# Loop through each CSV file and import into the database
for file in "${csv_files[@]}"
do
    table_name=$(basename "$file" .csv) # Extract table name from the CSV file name
    echo "Creating table $table_name from $file..."
    
    # Create table with id, src, and trg columns
    sqlite3 "$DB_NAME" <<EOF
CREATE TABLE IF NOT EXISTS $table_name (
    id INTEGER,
    src INTEGER,
    trg INTEGER
);
.separator ","
.import $file $table_name
EOF

    # Add indexes for each column
    id_index_name="id_index_${table_name}"
    src_index_name="src_index_${table_name}"
    trg_index_name="trg_index_${table_name}"
    sqlite3 "$DB_NAME" <<EOF
CREATE INDEX IF NOT EXISTS $id_index_name ON $table_name (id);
CREATE INDEX IF NOT EXISTS $src_index_name ON $table_name (src);
CREATE INDEX IF NOT EXISTS $trg_index_name ON $table_name (trg);
EOF
    
    echo "Imported data from $file into table $table_name and added indexes for columns."
done

echo "All CSV files imported into the database $DB_NAME successfully."
