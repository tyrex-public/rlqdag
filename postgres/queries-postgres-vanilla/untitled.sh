#!/bin/bash

# Variables
DB_NAME="yago"
MYSQL_USER="root"
MYSQL_PASSWORD="pass12"
HOST="localhost"
PORT="3306"
CSV_FOLDER="$1"

# Check if the database already exists
db_exists=$(mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" -e "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='$DB_NAME';" | grep "$DB_NAME" && echo "true" || echo "false")

# If database doesn't exist, create it
if [ "$db_exists" == "false" ]; then
    mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" -e "CREATE DATABASE $DB_NAME;"
    echo "Database $DB_NAME created."
else
    echo "Database $DB_NAME already exists."
fi

# Get all CSV files in the folder
csv_files=("$CSV_FOLDER"/*.csv)

# Loop through each CSV file and import into the database
for file in "${csv_files[@]}"
do
    table_name=$(basename "$file" .csv) # Extract table name from the CSV file name
    echo "Creating table $table_name from $file..."
    
    # Create table with id, src, and trg columns
    mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" -e "CREATE TABLE $table_name (id VARCHAR(50) PRIMARY KEY, src VARCHAR(50), trg VARCHAR(50));"
    
    # Import CSV data into the table
    mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" -e "LOAD DATA INFILE '$file' INTO TABLE $table_name FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;" 

    # Add indexes for each column
    id_index_name="id_index_${table_name}"
    src_index_name="src_index_${table_name}"
    trg_index_name="trg_index_${table_name}"
    mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" -e "CREATE INDEX $index_name ON $table_name (id);"
    mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" -e "CREATE INDEX $src_index_name ON $table_name (src);"
    mysql -h"$HOST" -P"$PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$DB_NAME" -e "CREATE INDEX $trg_index_name ON $table_name (trg);"
    
    echo "Imported data from $file into table $table_name and added indexes for columns."
done

echo "All CSV files imported into the database $DB_NAME successfully."
