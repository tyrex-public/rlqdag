CREATE TEMPORARY VIEW const_subquery2 AS
  (SELECT col2, col5 FROM (SELECT id, src AS col2, trg AS col5 FROM islocatedin) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation1_X1 (col3, col5) AS
    SELECT col3, col5 FROM (SELECT col3, col5 FROM (SELECT id, src AS col3, trg AS col5 FROM islocatedin) AS t) AS const
  UNION 
    SELECT col3, col5 FROM (SELECT col5, col3 FROM (SELECT * FROM (SELECT col3, col5 AS col2 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery2) AS t) AS rec;
CREATE TEMPORARY VIEW const_subquery4 AS
  (SELECT col4, trg FROM (SELECT id, src AS col4, trg FROM dealswith) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation3_X2 (col5, trg) AS
    SELECT col5, trg FROM (SELECT col5, trg FROM (SELECT id, src AS col5, trg FROM dealswith) AS t) AS const
  UNION 
    SELECT col5, trg FROM (SELECT trg, col5 FROM (SELECT * FROM (SELECT col5, trg AS col4 FROM fixpoint_relation3_X2) AS t NATURAL JOIN const_subquery4) AS t) AS rec;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, trg FROM (SELECT * FROM (SELECT x, col5 FROM (SELECT * FROM (SELECT x, col3 FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT id, src AS x, trg AS col1 FROM hasacademicadvisor) AS t) AS t1 NATURAL JOIN (SELECT col1, col3 FROM (SELECT id, src AS col1, trg AS col3 FROM livesin) AS t) AS t2) AS t) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation1_X1) AS t2) AS t) AS t1 NATURAL JOIN (SELECT * FROM fixpoint_relation3_X2) AS t2) AS t) AS t WHERE trg = '42262378') AS t) AS t;