CREATE TEMPORARY VIEW const_subquery3 AS
  (SELECT col2, trg FROM (SELECT id, src AS col2, trg FROM dealswith) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation2_X2 (col3, trg) AS
    SELECT col3, trg FROM (SELECT col3, trg FROM (SELECT id, src AS col3, trg FROM dealswith) AS t) AS const
  UNION 
    SELECT col3, trg FROM (SELECT trg, col3 FROM (SELECT * FROM (SELECT col3, trg AS col2 FROM fixpoint_relation2_X2) AS t NATURAL JOIN const_subquery3) AS t) AS rec;
CREATE TEMPORARY VIEW const_subquery4 AS
  (SELECT x, col1 FROM (SELECT id, src AS x, trg AS col1 FROM islocatedin) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation1_X1 (x) AS
    SELECT x FROM (SELECT x FROM (SELECT trg, x FROM (SELECT * FROM (SELECT * FROM (SELECT * FROM fixpoint_relation2_X2) AS t WHERE trg = '57917422') AS t1 NATURAL JOIN (SELECT x, col3 FROM (SELECT id, src AS x, trg AS col3 FROM islocatedin) AS t) AS t2) AS t) AS t) AS const
  UNION 
    SELECT x FROM (SELECT x FROM (SELECT * FROM (SELECT x AS col1 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery4) AS t) AS rec;
SELECT DISTINCT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t;