CREATE TEMPORARY VIEW const_subquery2 AS
  (SELECT y, col2 FROM (SELECT id, src AS y, trg AS col2 FROM islocatedin) AS t);
CREATE TEMPORARY VIEW const_subquery3 AS
  (SELECT col3, trg FROM (SELECT id, src AS col3, trg FROM dealswith) AS t);
DO $$BEGIN
CREATE TEMPORARY TABLE fixpoint_tmp AS
  (SELECT col4, y, trg FROM (SELECT * FROM (SELECT y, col4 FROM (SELECT id, src AS y, trg AS col4 FROM islocatedin) AS t) AS t1 NATURAL JOIN (SELECT col4, trg FROM (SELECT id, src AS col4, trg FROM dealswith) AS t) AS t2) AS t);
CREATE TEMPORARY TABLE fixpoint_relation1_X1 AS (SELECT * FROM fixpoint_tmp);
WHILE EXISTS (SELECT 1 FROM fixpoint_relation1_X1) LOOP
  CREATE TEMPORARY TABLE nouvelles AS
    (SELECT col4, y, trg FROM (SELECT y, col4, trg FROM (SELECT y, col4, trg FROM (SELECT * FROM (SELECT y AS col2, col4, trg FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery2) AS t) AS t1 UNION SELECT y, col4, trg FROM (SELECT trg, y, col4 FROM (SELECT * FROM (SELECT y, col4, trg AS col3 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery3) AS t) AS t2) AS rec_req EXCEPT SELECT * FROM fixpoint_tmp);
  INSERT INTO fixpoint_tmp (SELECT * FROM nouvelles);
  DROP TABLE fixpoint_relation1_X1;
  ALTER TABLE nouvelles RENAME TO fixpoint_relation1_X1;
END LOOP;
DROP TABLE fixpoint_relation1_X1;
ALTER TABLE fixpoint_tmp RENAME TO fixpoint_relation1_X1;
END;$$;
SELECT DISTINCT * FROM (SELECT x FROM (SELECT * FROM (SELECT x, y FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT id, src AS x, trg AS col1 FROM ismarriedto) AS t) AS t1 NATURAL JOIN (SELECT col1, y FROM (SELECT id, src AS col1, trg AS y FROM livesin) AS t) AS t2) AS t) AS t1 NATURAL JOIN (SELECT y FROM (SELECT * FROM (SELECT y, trg FROM (SELECT * FROM fixpoint_relation1_X1) AS t) AS t WHERE trg = '40324616') AS t) AS t2) AS t) AS t;