CREATE TEMPORARY VIEW const_subquery2 AS
  (SELECT x, col2 FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT id, src AS x, trg AS col1 FROM actedin) AS t) AS t1 NATURAL JOIN (SELECT col2, col1 FROM (SELECT id, src AS col2, trg AS col1 FROM actedin) AS t) AS t2) AS t);
CREATE TEMPORARY RECURSIVE VIEW fixpoint_relation1_X1 (x) AS
    SELECT x FROM (SELECT x FROM (SELECT x, trg FROM (SELECT * FROM (SELECT x, col1 FROM (SELECT id, src AS x, trg AS col1 FROM actedin) AS t) AS t1 NATURAL JOIN (SELECT * FROM (SELECT trg, col1 FROM (SELECT id, src AS trg, trg AS col1 FROM actedin) AS t) AS t WHERE trg = '99436162') AS t2) AS t) AS t) AS const
  UNION 
    SELECT x FROM (SELECT x FROM (SELECT * FROM (SELECT x AS col2 FROM fixpoint_relation1_X1) AS t NATURAL JOIN const_subquery2) AS t) AS rec;
SELECT DISTINCT * FROM (SELECT * FROM fixpoint_relation1_X1) AS t;